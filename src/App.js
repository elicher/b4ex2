// import React, {useState} from 'react';
// import DisplayResult from './components/DisplayResult';
// import UserInput from './components/UserInput';

// function App() {
//   const [userInput,setUserInput] = useState(0);
//   const handleChangeInput = event => setUserInput(event.target.value);
//   const count = (userInput) =>{
//     var result = userInput*1.17
//     if (!result>0) {
//       result=3.33
//     } 
//     return result}
//   return (
//     <div className="App">
//   <UserInput input={userInput} onChangeInput={handleChangeInput} />
//   <DisplayResult count = {count(userInput)} />
//   </div>
//   )
//   }

// export default App;


import React, {useState} from 'react';
import DisplayResult from './components/DisplayResult';
import UserInput from './components/UserInput';

function App() {
  const [userInput,setUserInput] = useState(0);
  const [rate,setRate] = useState(0);
  const handleChangeInput = event => setUserInput(event.target.value);
  const findCurrency = () => {
    fetch(`http://www.apilayer.net/api/live?access_key=aaca2d6ddb0e381b3b85c0f423beffaa`)
  .then((res) => res.json().then((check)=> {
    setRate(check.quotes.USDEUR)
}))
  .then((response) => console.log(response))
  .catch((error) => console.log(error));
  }
  const count = (userInput,rate) =>{
    findCurrency()
    var result = userInput/rate
    if (!result>0) {
      result=0;
    } 
    return result}
  return (
    <div className="App">
  <UserInput input={userInput} onChangeInput={handleChangeInput} />
  <DisplayResult count = {count(userInput,rate)} />
  </div>
  )
  }

export default App;