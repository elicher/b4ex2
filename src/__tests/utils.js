import React from "react";

const isFunctionalComponent = (child) => {
  return `${child.type}`.split(" ").slice(0, 4)[0] === "function" ||
    `${child.type}`.split(" ").slice(0, 4)[1] === "=>"
    ? true
    : false;
};

const isClassComponent = (child) => {
  return `${(<ClassComp />).type}`.split(" ").slice(0, 4)[0] ===
    `${child.type}`.split(" ").slice(0, 4)[0] &&
    `${(<ClassComp />).type}`.split(" ").slice(0, 4)[2] ===
      `${child.type}`.split(" ").slice(0, 4)[2] &&
    `${(<ClassComp />).type}`.split(" ").slice(0, 4)[3] ===
      `${child.type}`.split(" ").slice(0, 4)[3]
    ? true
    : false;
};

class ClassComp extends React.Component {
  render() {
    return null;
  }
}

module.exports = {
  isClassComponent,
  isFunctionalComponent,
};
