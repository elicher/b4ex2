// import React from "react";

// const UserInput = ({ input, onChangeInput }) => (
//     <div>
//         <h1>Your input below:</h1>
//       <input
//         type="text"
//         value={input}
//         onChange={onChangeInput}
//       />
//     </div>
// )

// export default UserInput

import React from "react";

const UserInput = ({ input, onChangeInput }) => (
    <div>
        <h1>Your input below:</h1>
      <input
        type="number"
        value={input}
        onChange={onChangeInput}
      />
    </div>
)

export default UserInput